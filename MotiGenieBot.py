from telegram.ext import Updater, Dispatcher

updater = Updater(token='6175833817:AAGTNwOxnE6I5N7zWFeezGrGBZy-EuqZfk8', use_context=True)
dispatcher = updater.dispatcher

user_info = {}

def start(update, context):
    user_id = update.effective_user.id
    
    if user_id not in user_info:
        user_info[user_id] = {}
    
    context.bot.send_message(chat_id=user_id, text="Привет! Как тебя зовут?")

def name(update, context):
    user_id = update.effective_user.id
    user_name = update.message.text
    
    user_info[user_id]['name'] = user_name
    
    if 'age' not in user_info[user_id]:
        context.bot.send_message(chat_id=user_id, text="Сколько тебе лет?")

def age(update, context):
    user_id = update.effective_user.id
    user_age = update.message.text
    
    user_info[user_id]['age'] = user_age
    
    context.bot.send_message(chat_id=user_id, text="Спасибо, я запомнил твои данные!")

import datetime

def get_time_of_day():
    current_hour = datetime.datetime.now().hour
    
    if current_hour >= 6 and current_hour < 12:
        return 'утро'
    elif current_hour >= 12 and current_hour < 18:
       

import openai
import telegram
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters

# Инициализация OpenAI API
openai.api_key = "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"

# Инициализация Telegram API
bot = telegram.Bot(token="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")

# Обработчик команды /start
def start(update, context):
    context.bot.send_message(chat_id=update.effective_chat.id, text="Привет! Я мотивационный бот. Как тебя зовут?")

# Обработчик текстовых сообщений
def process_text(update, context):
    # Получаем имя пользователя
    name = update.message.text.strip()

    # Спрашиваем возраст пользователя
    context.bot.send_message(chat_id=update.effective_chat.id, text="Сколько тебе лет?")

    # Устанавливаем имя пользователя в контексте
    context.user_data['name'] = name

# Обработчик текстовых сообщений
def process_age(update, context):
    # Получаем возраст пользователя
    age = update.message.text.strip()

    # Спрашиваем пол пользователя
    context.bot.send_message(chat_id=update.effective_chat.id, text="Какой у тебя пол?")

    # Устанавливаем возраст пользователя в контексте
    context.user_data['age'] = age

# Обработчик текстовых сообщений
def process_gender(update, context):
    # Получаем пол пользователя
    gender = update.message.text.strip()

    # Генерируем мотивационное сообщение на основе данных пользователя
    message = generate_message(context.user_data['name'], context.user_data['age'], gender)

    # Отправляем сообщение пользователю
    context.bot.send_message(chat_id=update.effective_chat.id, text=message)

# Функция для генерации мотивационных сообщений
def generate_message(name, age, gender):
    # Формируем запрос для GPT-3.5
    prompt = f"Привет, {name}! Тебе уже {age} {get_age_suffix(age)}."
    prompt += " Я хочу поделиться с тобой мотивационным сообщением, которое поможет тебе преодолеть трудности и достичь своих целей."
    prompt += " Ты готов? Пожалуйста, ответь на вопрос: что для тебя самое важное в жизни?"

    # Запрос к GPT-3.5
    response = openai.Completion.create(
        engine="text-davinci-002",
        prompt=prompt,
        max_tokens=256,
        n=1,
        stop=None,
        temperature=0.5,
    )

    # Получаем ответ от GPT-3.5
    message = response.choices[0].text.strip()

    # Добавляем завершающую фразу
    if

def generate_motivation_message(name, age, gender):
    prompt = f"Hello {name}, you are {age} years old and {gender}. Today is a new day and I believe in you! "
    generated_text = openai.Completion.create(
        engine="text-davinci-002",
        prompt=prompt,
        max_tokens=60,
        n=1,
        stop=None,
        temperature=0.7,
    ).choices[0].text
    return generated_text

def send_motivation_message(chat_id):
    name, age, gender = get_user_info(chat_id)
    message = generate_motivation_message(name, age, gender)
    bot.send_message(chat_id, message)
